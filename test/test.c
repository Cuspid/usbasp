#include <avr/io.h>
#include <util/delay.h>

int main()
{
    DDRB = (1<<PB1); // set PB1 as output
    PORTB = (1<<PB1);
    
    while(1)
    {
        _delay_ms(125);
        PORTB ^= (1<<PB1);
    }
}
